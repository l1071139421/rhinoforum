<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Tests\TestCase;
use App\Models\User;
use App\Models\Post;

class PostRouteTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * 基礎測試
     */
    public function test_a_basic_request()
    {
        $response = $this->get('/api/posts');

        $response
            ->assertStatus(200)
            ->assertExactJson([
                'data' => [],
                'current_page' => 1,
                'per_page' => 15,
                'total' => 0,
                'last_page' => 0,
            ]);
    }

    /**
     * 測試搜尋一個使用者發佈的文章
     */
    public function test_search_an_user_request()
    {
        User::factory()
            ->has(Post::factory()->count(1))
            ->create();
        $user = User::factory()
            ->has(Post::factory()->count(1))
            ->create();

        $response = $this->get('/api/posts?user_id=' . $user->id);
        $response
            ->assertStatus(200)
            ->assertExactJson([
                'data' => $user->posts->toArray(),
                'current_page' => 1,
                'per_page' => 15,
                'total' => 1,
                'last_page' => 1,
            ]);
    }

    /**
     * 測試搜尋錯誤格式的user_id
     */
    public function test_search_invalid_format_of_user_id()
    {
        // 測試邊界值
        $response = $this->get('/api/posts?user_id=0');
        $response
            ->assertStatus(422)
            ->assertExactJson([
                'errors' => [
                    'user_id' => [
                        'The user id must be at least 1.',
                    ],
                ],
            ]);

        // 測試字串
        $response = $this->get('/api/posts?user_id=a');
        $response
            ->assertStatus(422)
            ->assertExactJson([
                'errors' => [
                    'user_id' => [
                        'The user id must be an integer.',
                    ],
                ],
            ]);

        // 測試負數
        $response = $this->get('/api/posts?user_id=-1');
        $response
            ->assertStatus(422)
            ->assertExactJson([
                'errors' => [
                    'user_id' => [
                        'The user id must be at least 1.',
                    ],
                ],
            ]);
    }

    /**
     * 測試搜尋發佈時間區間內的文章
     */
    public function test_search_published_at_range_request()
    {
        $user = User::factory()
            ->has(
                Post::factory()
                    ->count(2)
                    ->state(new Sequence(
                        [ 'published_at' => '2021-10-17 02:26:30' ],
                        [ 'published_at' => '2021-10-17 02:26:31' ],
                    ))
            )
            ->create();

        $response = $this->get('/api/posts?begin_at=2021-10-17T10:26:00%2B08:00&end_at=2021-10-17T10:26:30%2B08:00');

        $response
            ->assertStatus(200)
            ->assertExactJson([
                'data' => [ $user->posts->toArray()[0] ],
                'current_page' => 1,
                'per_page' => 15,
                'total' => 1,
                'last_page' => 1,
            ]);
    }

    /**
     * 測試搜尋發布時間區間的開始時間晚於結束時間
     */
    public function test_published_begin_at_be_a_date_after_published_end_at()
    {
        $response = $this->get('/api/posts?begin_at=2021-10-17T10:26:00%2B08:00&end_at=2021-10-16T23:26:30%2B08:00');
        $response
            ->assertStatus(422)
            ->assertExactJson([
                'errors' => [
                    'begin_at' => [
                        'The begin at must be a date before or equal to end at.',
                    ],
                    'end_at' => [
                        'The end at must be a date after or equal to begin at.',
                    ],
                ],
            ]);
    }

    /**
     * 測試搜尋錯誤格式的時間格式
     */
    public function test_serach_invalid_format_of_published_at()
    {
        $response = $this->get('/api/posts?begin_at=2021-10-17%2009:26:00');
        $response
            ->assertStatus(422)
            ->assertExactJson([
                'errors' => [
                    'begin_at' => [
                        'The begin at does not match the format Y-m-d\TH:i:sP.',
                        'The begin at must be a date before or equal to end at.',
                    ],
                ],
            ]);
    }

    /**
     * 測試依類別搜尋文章
     */
    public function test_search_category_request()
    {
        $user = User::factory()
            ->has(
                Post::factory()
                    ->count(2)
                    ->state(new Sequence(
                        [ 'category' => 'Financial' ],
                        [ 'category' => 'Startup' ],
                    ))
            )
            ->create();

        $response = $this->get('/api/posts?category[]=Financial');

        $response
            ->assertStatus(200)
            ->assertExactJson([
                'data' => [ $user->posts->toArray()[0] ],
                'current_page' => 1,
                'per_page' => 15,
                'total' => 1,
                'last_page' => 1,
            ]);
    }

    /**
     * 測試錯誤格式的類別參數
     */
    public function test_search_invalid_format_of_category()
    {
        $response = $this->get('/api/posts?category=Financial');
        $response
            ->assertStatus(422)
            ->assertExactJson([
                'errors' => [
                    'category' => [
                        'The category must be an array.'
                    ],
                ],
            ]);
    }

    /**
     * 測試錯誤資料的類別
     */
    public function test_invalid_value_of_category_request()
    {
        $response = $this->get('/api/posts?category[]=Abc');
        $response
            ->assertStatus(422)
            ->assertExactJson([
                'errors' => [
                    'category' => [
                        'The selected category is invalid.'
                    ],
                ],
            ]);
    }

    /**
     * 測試搜尋文章內容
     */
    public function test_search_content()
    {
        $user = User::factory()
            ->has(
                Post::factory()
                    ->count(2)
                    ->state(new Sequence(
                        [ 'content' => 'Financial' ],
                        [ 'content' => 'Startup' ],
                    ))
            )
            ->create();

        $response = $this->get('/api/posts?content=Fin');

        $response
            ->assertStatus(200)
            ->assertExactJson([
                'data' => [ $user->posts->toArray()[0] ],
                'current_page' => 1,
                'per_page' => 15,
                'total' => 1,
                'last_page' => 1,
            ]);
    }

    /**
     * 測試搜尋中文
     */
    public function test_search_content_by_other_langugage()
    {
        $user = User::factory()
            ->has(
                Post::factory()
                    ->count(2)
                    ->state(new Sequence(
                        [ 'content' => '你好' ],
                        [ 'content' => '世界' ],
                    ))
            )
            ->create();

        $response = $this->get('/api/posts?content=你');

        $response
            ->assertStatus(200)
            ->assertExactJson([
                'data' => [ $user->posts->toArray()[0] ],
                'current_page' => 1,
                'per_page' => 15,
                'total' => 1,
                'last_page' => 1,
            ]);
    }

    /**
     * 測試sql injection
     */
    public function test_sql_injection_for_content()
    {
        $response = $this->get('/api/posts?content=;select%201;');
        $response
            ->assertStatus(200)
            ->assertExactJson([
                'data' => [],
                'current_page' => 1,
                'per_page' => 15,
                'total' => 0,
                'last_page' => 0,
            ]);
    }

    /**
     * 測試分頁
     */
    public function test_pagination_request()
    {
        $user = User::factory()
            ->has(Post::factory()->count(20))
            ->create();
        $posts = array_slice($user->posts->toArray(), 12);
        $response = $this->get('/api/posts?page=2&per_page=12');
        $response
            ->assertStatus(200)
            ->assertExactJson([
                'data' => $posts,
                'current_page' => 2,
                'per_page' => 12,
                'total' => 20,
                'last_page' => 2,
            ]);
    }
}
