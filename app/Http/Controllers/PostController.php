<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use App\Models\Post;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * 取得文章
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getPosts(Request $request)
    {
        $rules = [
            'user_id' => [ 'integer', 'min:1' ],
            'category' => [
                'array',
                Rule::in(['Tech', 'Financial', 'Startup', 'Design']),
            ],
            'content' => [ 'string', 'min:1' ],
            'begin_at' => [ 'date_format:Y-m-d\TH:i:sP', 'before_or_equal:end_at' ],
            'end_at' => [ 'date_format:Y-m-d\TH:i:sP', 'after_or_equal:begin_at' ],
            'page' => [ 'integer', 'min:1' ],
            'per_page' => [ 'integer', 'min:1' ],
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([ 'errors' => $validator->errors() ], 422);
        }

        $validated = $validator->validated();

        $stmt = new Post();
        if (isset($validated['user_id'])) {
            $stmt = $stmt->where('user_id', $validated['user_id']);
        }

        if (isset($validated['category'])) {
            $stmt = $stmt->whereIn('category', $validated['category']);
        }

        if (isset($validated['content'])) {
            $stmt = $stmt->where('content', 'like', '%' . $validated['content'] . '%');
        }

        if (isset($validated['begin_at']) && isset($validated['end_at'])) {
            $beginAt = (new \DateTime($validated['begin_at']))->setTimezone(new \DateTimeZone(date_default_timezone_get()));
            $endAt = (new \DateTime($validated['end_at']))->setTimezone(new \DateTimeZone(date_default_timezone_get()));
            $stmt = $stmt->whereBetween('published_at', [ $beginAt, $endAt ]);
        }

        $total = $stmt->count();
        $perPage = (int)($validated['per_page'] ?? 15);
        $page = (int)($validated['page'] ?? 1);
        $lastPage = (int)ceil($total/$perPage);
        if ($total > 0) {
            $offset = ($page - 1) * $perPage;
            $stmt = $stmt->orderBy('id', 'asc');
            $stmt = $stmt->offset($offset);
            $stmt = $stmt->limit($perPage);

            $posts = $stmt->get();
        }
        return [
            'data' => !isset($posts) ? [] : $posts->toArray(),
            'current_page' => $page,
            'per_page' => $perPage,
            'total' => $total,
            'last_page' => $lastPage,
        ];
    }
}
